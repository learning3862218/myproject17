﻿#include <iostream>

class Vector
{
private:
    double _x, _y, _z;

public:
    Vector(): _x(0.0), _y(0.0), _z(0.0) {}

    Vector(double x, double y, double z) : _x(x), _y(y), _z(z) {}

    void SetCoords(double x, double y, double z)
    {
        _x = x;
        _y = y;
        _z = z;
    }

    Vector GetCoords()
    {
        return Vector(_x, _y, _z);
    }

    double Length()
    {
        return sqrt(_x * _x + _y * _y + _z * _z);
    }
};

int main()
{
    Vector a(1, 1, 1);
    Vector b;
    Vector c(2, 5, 10);

    std::cout << "Length of vector a: " << a.Length() << std::endl;
    std::cout << "Length of vector b: " << b.Length() << std::endl;
    std::cout << "Length of vector c: " << c.Length() << std::endl;
}
